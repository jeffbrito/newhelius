<?php
session_start();
require_once("../Controle/UsuarioControle.php");
require_once("../Controle/Conexao.php");
require_once("../Controle/SiteControle.php");
$Scontrol = new SiteControle();
$lista = $Scontrol->ReadSite();
if($lista != NULL){
    echo "
        <!Doctype html>
        <html lang='pt-br'>
            <head>
                <meta charset='utf-8' />
                <link rel='stylesheet' type='text/css' href='css/main.css'/>
                <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                <link rel='stylesheet' type='text/css' href='css/materialize.css' />
                <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'>
            </head>
            <body> 
                <!--Navbar -->
                    <nav style='background-color:#000;'> 
                    <a href='#' data-target='slide-out' class='sidenav-trigger'><i class='material-icons'>menu</i></a>
                    <div class='nav-wrapper'>
                        <a href='' class='brand-logo'><img src='Imagens/Helius_Logo.svg' width='70px' height='70px'/> </a>
                        <ul id='nav-mobile' class='right hide-on-med-and-down'>
                            <li><a href='../index.php'><i class='material-icons'>person_add</i>Cadastro</a></li>
                            <li><a href='Sobre.php'><i class='material-icons'>book</i>Sobre</a></li>
                            <li><a href='Config.php'><i class='material-icons'>build</i>Configurações</a></li>
                            <li><a href='Sair'><i class='material-icons'>exit_to_app</i>Sair</a></li>
                        </ul>
                    </div>
                </nav>

                <!--funcionamento do sidenav-->
                <ul id='slide-out' class='sidenav'>
                    <li>
                        <div class='user-view'>
                            <div class='background' style='background-color:#000'>
                            </div>
                            <a href='index.php'><img class='circle' src='Imagens/Helius_Logo.svg'></a>
                            <a href='index.php'><span class='white-text name'>{$_SESSION['nome']}</span></a>
                            <a href='index.php'><span class='white-text email'>{$_SESSION['email']}</span></a>

                        </div>
                    </li>
                    <li><a href='../index.php'><i class='material-icons'>person_add</i>Cadastro</a></li>
                    <li><a href='Sobre.php'><i class='material-icons'>book</i>Sobre</a></li>
                    <li><a href='Config.php'><i class='material-icons'>build</i>Configurações</a></li>
                    <li><a href='Sair'><i class='material-icons'>exit_to_app</i>Sair</a></li>
                </ul>
        ";
    foreach($lista as $item){
        echo"
                <!--Corpo-->
                <div class='row'>
                    <h3 class='col s12'>Seus Sites em funcionamento </h3>
                </div>
            <div class='container'>
                <div class='row'>
                    <div class='quadro col s12'>
                        <a href='SiteP.php' style='color:#000;'><h5>{$item->getNome_site()}(Site padrão)</h5>
                    </div>
                    <div class='funcoes col s12'>
                        <h4><a href='CriarSIte.php'>Criar site</a><h4>
                            <h5>Crie um site no seu estilo, adicione texto,redes sociais e entre outras coisas.</h5>
                        <h4><a href='#'>Deletar site</a><h4>
                            <h5>Caso tenha algum problema, ou decida de vez não utilizar algum site seu criado, pode deletar ele.</h5>
                        <h4><a href='#'>Atualizar site</a></h4>
                            <h5> É sempre bom manter seu site atualizado, então não se esqueça.</h5>

                    </div>
                </div>
            </div>
                <script src='js/jquery.js'></script>
                <script type='text/javascript' src='js/materialize.js'></script>
                <script>
                    $(document).ready(function(){
                        $('.sidenav').sidenav();
                    });
                </script>  
            </body>
        </html>  
    ";
    }
}
?>