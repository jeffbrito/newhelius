<?php
/*
@name: Helius
@author: Jefferson Brito de oliveira
@version: 0.0.1
*/
session_start();
if(isset($_SESSION['id'])){
    header('Location: Visual/Main.php');
}
        echo"
            <!Doctype html>
            <html lang='pt-br'>
                <head>
                    <meta charset='utf-8'/>
                        <link rel='stylesheet' type='text/css' href='Visual/css/ind.css'/>
                        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                        <link rel='stylesheet' type='text/css' href='Visual/css/materialize.css' />
                        <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'>

                </head>
           
                <body>
                            <nav style='background-color:#000;'> 
                                <a href='#' data-target='slide-out' class='sidenav-trigger'><i class='material-icons'>menu</i></a> 
                                <div class='nav-wrapper'>
                                    <a href='' class='brand-logo'><img src='Visual/Imagens/Helius_Logo.svg' width='70px' height='70px'/> </a>
                                    <ul id='nav-mobile' class='right hide-on-med-and-down'>
                                        <li><a href='Visual/Login.php'>Login</a>
                                        <li><a href='Visual/Sobre.php'>Sobre</a>

                                    </ul>
                                </div>
                            </nav>

                            <ul id='slide-out' class='sidenav'>
                                <li>
                                    <div class='user-view'>
                                        <div class='background' style='background-color:#000'>
                                        </div>
                                        <a href='index.php'><img class='circle' src='Visual/Imagens/Helius_Logo.svg'></a>
                                        <a href='index.php'><span class='white-text name'>Hélius</span></a>
                                    </div>
                                </li>
                                <li><a href='Visual/Login.php'><i class='material-icons'>person</i>Login</a></li>
                                <li><a href='Visual/Sobre.php'><i class='material-icons'>book</i>Sobre</a></li>
                            </ul>


                            <div class='container top'>
                                <div class='logo'>
                                    <img class='responsive-image' src='Visual/Imagens/Helius_Logo.svg' alt='Logo_Helius' width='200px' height='200px'/>
                                    <h4 class='h'> Hélius </h4>
                                    
                                </div>
                            </div>
                            
                        
                            <div class='container'>
                                    <h3 class='cad-title'> Cadastre-se </h3>
                                    <form class='cadastro' method='post' action='Controle/Add.php'>
                                        <label class='lab-name lab'> Nome </label>
                                            <input class='inp' type='text' name='nome' />
                                        <label class='lab-email lab'> Email </label>
                                            <input class='inp' type='email' name='email' />
                                        <label class='lab-senha lab'> senha </label>
                                            <input class='inp' type='password' name='senha' />
                                        <label class='lab-consenha lab'> Confirmação de senha </label>
                                            <input class='inp' type='password' name='consenha' />
                                        <div class='row'>
                                            <div class='input-field col' style='display:flex; align-items:center; justify-content:center;'>
                                                <input class='btn' type='submit' value='Enviar' />
                                                <p>Caso já tenha conta:<a href='Visual/Login.php' id='login'>Login</a></p>
                                            </div>
                                        </div>
                                    </form>
                            </div>
                        <script src='Visual/js/jquery.js'></script>
                        <script type='text/javascript' src='Visual/js/materialize.js'></script>
                        <script>
                            $(document).ready(function(){
                                $('.sidenav').sidenav();
                            });
                        </script>
                </body>
                </html>
";
        
?>