<?php
session_start();
 echo"
 <!Doctype html>
 <html lang='pt-br'>
     <head>
         <meta charset='utf-8'/>
             <link rel='stylesheet' type='text/css' href='css/ind.css'/>
             <meta name='viewport' content='width=device-width, initial-scale=1.0'>
             <link rel='stylesheet' type='text/css' href='css/materialize.css' />
             <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'>

     </head>

     <body>
                 <nav style='background-color:#000;'> 
                     <a href='#' data-target='slide-out' class='sidenav-trigger'><i class='material-icons'>menu</i></a>
                     <div class='nav-wrapper'>
                         <a href='' class='brand-logo'><img src='Imagens/Helius_Logo.svg' width='70px' height='70px'/> </a>
                         <ul id='nav-mobile' class='right hide-on-med-and-down'>
                             <li><a href='../index.php'>Cadastro</a>
                             <li><a href='Sobre.php'>Sobre</a>
                         </ul>
                     </div>
                 </nav>

                 <ul id='slide-out' class='sidenav'>
                     <li>
                         <div class='user-view'>
                             <div class='background' style='background-color:#000'>
                             </div>
                             <a href='../index.php'><img class='circle' src='Imagens/Helius_Logo.svg'></a>
                             <a href='../index.php'><span class='white-text name'>Hélius</span></a>
                         </div>
                     </li>
                     <li><a href='../index.php'><i class='material-icons'>person_add</i>Cadastro</a></li>
                     <li><a href='Sobre.php'><i class='material-icons'>book</i>Sobre</a></li>
                 </ul>

                <form method='post' action='../Controle/AddSite.php'>
                    <h3> Crie o seu site </h3>
                    <div class='row'>
                        <div class='input-field col s6'>
                             <label for='nome_site'> Adicione um nome ao seu site </label>
                             <input type='text' id='nome_site' name='nome_site'/>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='input-field col s6'>
                            <label for='textarea1'> Adicione o texto 1 se quiser </label>
                            <textarea id='textarea1' class='materialize-textarea' name='texto_um'></textarea>
                        </div>
                    </div>

                    <div class='row'>
                        <div class='input-field col s6'>
                             <label for='textarea2'> Adicione o texto 2 se quiser</label>
                             <textarea id='textarea2' class='materialize-textarea' name='texto_dois'></textarea>

                        </div>
                    </div>

                    <div class='row'>
                        <div class='input-field col s6'>
                             <label for='textarea3'>Adicione o texto 3 se quiser </label>
                             <textarea id='textarea3' class='materialize-textarea' name='texto_tres'></textarea>

                        </div>
                    </div>

                    <div class='row'>
                        <div class='input-field col s6'>
                             <label for='email_site'>Adicione seu email</label>
                             <input type='email' id='email_site' name='email_site'/>
                        </div>
                    </div>

                    <div class='row'>
                        <div class='input-field col s6'>
                             <label for='whatsapp'>Adicione seu whatsapp</label>
                             <input type='text' id='whatsapp' name='whatsapp'/>
                        </div>
                    </div>

                    <div class='row'>
                        <div class='input-field col s6'>
                             <label for='cor_texto'>Adicione uma cor para o texto</label>
                             <input type='color' id='cor_texto' name='cor_texto'/>
                        </div>
                    </div>

                    <div class='row'>
                        <div class='input-field col s6'>
                            <label for='cor_fundo'>Adicione uma cor para o fundo do seu site</label>
                            <input type='color' id='cor_fundo' name='cor_fundo'/>
                        </div>
                    </div>

                    <div class='row'>
                        <div class='input-field col s6'>
                            <input class='btn' type='submit' value='Criar'/>
                        </div>
                 </div>
            
                </form>

                 <script src='js/jquery.js'></script>
                 <script type='text/javascript' src='js/materialize.js'></script>
                 <script>
                     $(document).ready(function(){
                         $('.sidenav').sidenav();
                     });
                 </script>
                 <script>
                    $('#textarea1').val('New Text');
                    M.textareaAutoResize($('#textarea1'));

                    $('#textarea2').val('New Text');
                    M.textareaAutoResize($('#textarea2'));

                    $('#textarea3').val('New Text');
                    M.textareaAutoResize($('#textarea3'));
                 </script>                          
     </body>
     </html>
";      
?>