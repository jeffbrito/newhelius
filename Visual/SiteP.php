<?php
session_start();
require_once("../Controle/SiteControle.php");
require_once("../Controle/Conexao.php");
$Scontrol = new SiteControle();
$lista = $Scontrol->ReadSite();
if($lista != NULL){
    echo "
        <!Doctype html>
        <html lang='pt-br'>
            <head>
                <meta charset='utf-8' />
                <link rel='stylesheet' type='text/css' href='css/main.css'/>
                <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                <link rel='stylesheet' type='text/css' href='css/materialize.css' />
                <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'>
            </head>
            <body>
            
                <!--Navbar -->
                    <nav style='background-color:#000;'> 
                    <a href='#' data-target='slide-out' class='sidenav-trigger'><i class='material-icons'>menu</i></a>
                    <div class='nav-wrapper'>
                        <a href='' class='brand-logo'><img src='Imagens/Helius_Logo.svg' width='70px' height='70px'/> </a>
                        <ul id='nav-mobile' class='right hide-on-med-and-down'>
                            <li><a href='../index.php'><i class='material-icons'>person_add</i>Cadastro</a></li>
                            <li><a href='Sobre.php'><i class='material-icons'>book</i>Sobre</a></li>
                            <li><a href='Config.php'><i class='material-icons'>build</i>Configurações</a></li>
                            <li><a href='Sair'><i class='material-icons'>exit_to_app</i>Sair</a></li>
                        </ul>
                    </div> 
                </nav>

                <!--funcionamento do sidenav-->
                <ul id='slide-out' class='sidenav'>
                    <li>
                        <div class='user-view'>
                            <div class='background' style='background-color:#000'>
                            </div>
                            <a href='../index.php'><img class='circle' src='Imagens/Helius_Logo.svg'></a>
                            <a href='../index.php'><span class='white-text name'>{$_SESSION['nome']}</span></a>
                            <a href='../index.php'><span class='white-text email'>{$_SESSION['email']}</span></a>
                        </div>
                    </li>
                    <li><a href='../index.php'><i class='material-icons'>person_add</i>Cadastro</a></li>
                    <li><a href='Sobre.php'><i class='material-icons'>book</i>Sobre</a></li>
                    <li><a href='Config.php'><i class='material-icons'>build</i>Configurações</a></li>
                    <li><a href='Sair'><i class='material-icons'>exit_to_app</i>Sair</a></li>
                </ul>
    ";
    foreach($lista as $item){
        echo "
                <!--Corpo do site -->
                <div  class='container img'>
                    <h3 style='position:absolute;color:#fff;'>{$item->getNome_site()}</h3>
                </div>  
                <div class='container'>
                    <div class='row'>
                        <div class='texto col s12'>
                            <h4>{$item->getTexto_um()}</h4>
                        </div>
                    </div>
                    <div class='row'>  
                        <div class='texto col s12'>
                            <h4>{$item->getTexto_dois()}</h4> 
                        </div>
                    </div>
                    <div class='row'>
                        <div class='texto col s12'>
                            <h4>{$item->getTexto_tres()}</h4>
                        </div>
                    </div>
                </div>

                <div class='container footer'>
                    <div class='row'>
                            <div class='col s6'>
                                <img class='responsive-img' src='Imagens/gmail.png' width='50px' height='50px' />
                                <a href='{$item->getEmail_site()}'><p>{$item->getEmail_site()}</p></a>
                            </div>
                    
                            <div class='col s6'>
                                <img class='responsive-img' src='Imagens/whatsapp.png' width='50px' height='50px' />
                                <a href='{$item->getWhatsapp()}'><p>{$item->getWhatsapp()}</p></a>
                            </div>
                    </div>
                </div>
                <!--Scripts -->
                <script src='js/jquery.js'></script>
                <script type='text/javascript' src='js/materialize.js'></script>
                <script>
                    $(document).ready(function(){
                        $('.sidenav').sidenav();
                    });
                </script>

                <style>
                    html,body{
                        color:#000;
                        background-color:#fff;
                    }
                    .img{
                        width:100%;
                        height:400px;
                        background-image: url('Imagens/Helius_Logo.svg');
                        background-repeat: no-repeat;
                        background-size: cover;
                        background-attachment:fixed;
                        background-position: center;
                    }
                </style>
        </body>
        </html>
         ";
    }
}
?>  