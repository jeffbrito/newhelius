<?php
class ImgFundo{
    private $name_img;
    private $tipo_img;
    private $tmp_img;

    //getters
    public function getName_img(){
        return $this->name_img;
    }
    public function getTipo_img(){
        return $this->tipo_img;
    }
    public function getTmp_img(){
        return $this->tmp_img;
    }
    //setters
    public function setName_img($ni){
        $this->name_img = $ni;
    }
    public function setTipo_img($tim){
        $this->tipo_img = $tim;
    }
    public function setTmp_img($tmi){
        $this->tmp_img = $tmi;
    }
}
?>