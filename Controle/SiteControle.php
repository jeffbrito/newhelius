<?php
require_once('Conexao.php');
require_once('../Modelo/Site.php');
require_once('../Modelo/ImgFundo.php');
class SiteControle{
    //Criar Base do site
    function createBase($site){
        try{
            $conexao = new Conexao();
            $nome_site = $site->getNome_site();
            $texto_um = $site->getTexto_um();
            $texto_dois = $site->getTexto_dois();
            $texto_tres = $site->getTexto_tres();
            $email_site = $site->getEmail_site();
            $whatsapp = $site->getWhatsapp();
            $cor_texto = $site->getCor_texto();
            $cor_fundo = $site->getCor_fundo();
            $query = $conexao->getConexao()->prepare("INSERT INTO sites(nome_site,texto_um,texto_dois,texto_tres,email_site,whatsapp,cor_texto,cor_fundo) VALUES(:ns,:tum,:tdo,:ttr,:ems,:wh,:ct,:cf);");
            $query->bindParam("ns",$nome_site);
            $query->bindParam("tum",$texto_um);
            $query->bindParam("tdo",$texto_dois);
            $query->bindParam("ttr",$texto_tres);
            $query->bindParam("ems",$email_site);
            $query->bindParam("wh",$whatsapp);
            $query->bindParam("ct",$cor_texto);
            $query->bindParam("cf",$cor_fundo);
            if($query->execute()){
                $conexao->closeConection();
                return true;
            }else{
                $conexao->closeConection();
                return false;
            }
        }catch(PDOException $e){
            echo "Erro na criação base do site:{$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo "Erro geral na criação base do site:{$e->getMessage()}";
            return false;
        }
    }

    function ReadSite(){
        try{
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT * FROM sites;");
            $cmd->execute();
            $leitura = $cmd->fetchAll(PDO::FETCH_CLASS, "Site");
            return $leitura;
        }catch(PDOException $e){
            echo "Erro de pdo ao ler dados do site: {$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo "Erro generalizado:{$e->getMessage()}";
            return false;
        } 
        return true;
    }
    /*
    function updateBase($id_site,$site){
        try{
            $conexao = new Conexao();
            $nome_site = $site->getNome_site();
            $texto_um = $site->getTexto_um();
            $texto_dois = $site->getTexto_dois();
            $texto_tres = $site->getTexto_tres();
            $email_site = $site->getEmail_site();
            $whats = $site->getWhasapp();
            $cor_texto = $site->getCor_texto();
            $cor_fundo = $site->getCor_fundo();
            $query = $conexao->getConexao()->prepare("UPDATE sites SET nome_site = :ns, texto_um = :tum, texto_dois = :tdo, texto_tres = :ttr, email_site = :ems, whatsapp = :wh, cor_texto = :ct, cor_fundo = :cf;"); 
            $query->bindParam("ns",$nome_site);
            $query->bindParam("tum",$texto_um);
            $query->bindParam("tdo",$texto_dois);
            $query->bindParam("ttr",$texto_tres);
            $query->bindParam("ems",$email_site);
            $query->bindParam("ins",$insta);
            $query->bindParam("wh",$whats);
            $query->bindParam("ct",$cor_texto);
            $query->bindParam("cf",$cor_fundo);
            if($query->execute()){
                $conexao->closeConection();
                return true;
            }else{
                $conexao->closeConection();
                return false;
            }
        }catch(PDOException $e){
            echo "Erro na criação base do site:{$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo "Erro geral na criação base do site:{$e->getMessage()}";
            return false;
        }
    }
    
    function remover_site($id_site){
		try{
			$conexao = new Conexao();
			$cmd = $conexao->getConexao()->prepare("DELETE FROM client WHERE id_site = :ids");
			$cmd->bindParam("ids",$id_site);
			if($cmd->execute()){
			    $conexao->fecharConexao();
			    return true;			   
			}else{
			    $conexao->fecharConexao();
			    return false;
			}	
		}catch(PDOException $e){
			echo "Erro de pdo ao deletar dados do cliente:{$e->getMessage()}";
		}catch(Exception $e){
			echo "Erro geral:{$e->getMessage()}";
		}
    }
    */
}
?>