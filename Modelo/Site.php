<?php
class Site{
    private $id_site;
    private $nome_site;
    private $texto_um;
    private $texto_dois;
    private $texto_tres;
    private $email_site;
    private $whatsapp;
    private $cor_texto;
    private $cor_fundo;
    //getters
    public function getId_site(){
        return $this->id_site;
    }
    public function getNome_site(){
        return $this->nome_site;
    }
    public function getTexto_um(){
        return $this->texto_um;
    }
    public function getTexto_dois(){
        return $this->texto_dois;
    }
    public function getTexto_tres(){
        return $this->texto_tres;
    }
    public function getEmail_site(){
        return $this->email_site;
    }
    public function getWhatsapp(){
        return $this->whatsapp;
    }
    public function getCor_texto(){
        return $this->cor_texto;
    }
    public function getCor_fundo(){
        return $this->cor_fundo;
    }
    //setters
    public function setId_site($is){
        $this->id_site = $is;
    }
    public function setNome_site($ns){
        $this->nome_site = $ns;
    }
    public function setTexto_um($tu){
        $this->texto_um = $tu;
    }
    public function setTexto_dois($td){
        $this->texto_dois = $td;
    }
    public function setTexto_tres($tt){
        $this->texto_tres = $tt;
    }
    public function setEmail_site($es){
        $this->email_site = $es;
    }
    public function setWhatsapp($wh){
        $this->whatsapp = $wh;
    }
    public function setCor_texto($ct){
        $this->cor_texto = $ct;
    }
    public function setCor_fundo($cf){
        $this->cor_fundo = $cf;
    }
}
?>