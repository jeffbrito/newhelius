<?php
require_once("Conexao.php");
require_once("../Modelo/Client.php");
class UsuarioControle{
    //Create
    function create($client){
		try{
	      	$conexao = new Conexao();
		    $nome = $client->getNome();
		    $email = $client->getEmail();
		    $senha = $client->getSenha();
		    $query = $conexao->getConexao()->prepare("INSERT INTO client(nome,email,senha) VALUES(:n,:e,:s);");
		    $query->bindParam("n",$nome);
		    $query->bindParam("e",$email);
		    $query->bindParam("s",$senha);
		    if($query->execute()){
                $conexao->closeConection();
                return true;
		    }else{
                $conexao->closeConection();
                return false;
		    }
		    }catch(PDOException $e){
			    echo "Erro em pdo:{$e->getMessage()}";
			    return false;
		    }catch(Exception $e){
			    echo "Erro geral:{$e->getMessage()}";
			    return false;
		 }
	}
    
    //Read
    function Read(){
        try{
            $conexao = new Conexao();
            $cmd = $conexao->getConexao->prepare("SELECT * FROM client");
            $cmd->execute();
            $leitura = $cmd->fetchAll(PDO::FETCH_CLASS, "Client");
            return $leitura;
        }catch(PDOException $e){
            echo "Erro de pdo ao ler dados do cliente: {$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo "Erro generalizado:{$e->getMessage()}";
            return false;
        } 
        return true;
    }
    //ReadId
    function ReadId($e,$s){
	    try{
	        $conexao = new Conexao();
	        $cmd = $conexao->getConexao()->prepare("SELECT id,nome,email,senha FROM client WHERE email = :e AND senha = :s;");
	        $cmd->bindParam("e",$e);
	        $cmd->bindParam("s",$s);
	        if($cmd->execute()){
                $result = $cmd->fetchAll(PDO::FETCH_CLASS,"Client");
                return $result;
            }
	    }catch(PDOException $e){
            echo "Erro no pdo:{$e->getMessage()}";
            return false;
	    }catch(Exception $e){
            echo "Erro geral:{$e->getMessage()}";
            return false;
	    }	    
	}
    //ReadUnique
    function readUnique($e,$s){
	    try{
	        $conexao = new Conexao();
	        $cmd = $conexao->getConexao()->prepare("SELECT * FROM client WHERE email = :e AND senha = :s;");
	        $cmd->bindParam("e",$e);
	        $cmd->bindParam("s",$s);
	        if($cmd->execute()){
	            if($cmd->rowCount() == 1){
	                return true;
	            }else{
	                return false;
	            }
	        }
	    }catch(PDOException $e){
	        echo "Erro em pdo:{$e->getMessage()}";
	    }catch(Exception $e){
	        echo "Erro geral:{$e->getMessage()}";
	    }
	    
	}

    //Update
    function Update($id,$client){
        try{
            $conexao = new Conexao();
            $nome = $cliente->getNome();
            $email = $client->getEmail();
            $senha = $client->getSenha();
            $query = $conexao->getConexao()->prepare("UPDATE client SET nome = :n, email = :e, senha = :s WHERE id = :id ;");
            $query->bindParam("n",$nome);
            $query->bindParam("e",$email);
            $query->bindParam("s",$senha);
            $query->bindParam("id",$id);
            if($query->execute()){
                $conexao->closeConection();
                return true;
            }else{
                $conexao->closeConection();
                return false;
            }
        }catch(PDOException $e){
            echo "Erro de pdo ao atualizar dados do cliente: {$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo "Erro generalizado:{$e->getMessage()}";
            return false;
        }  
    }
    //Delete
    function remover($id){
		try{
			$conexao = new Conexao();
			$cmd = $conexao->getConexao()->prepare("DELETE FROM client WHERE id=:id");
			$cmd->bindParam("id",$id);
			if($cmd->execute()){
			    $conexao->fecharConexao();
			    return true;			   
			}else{
			    $conexao->fecharConexao();
			    return false;
			}	
		}catch(PDOException $e){
			echo "Erro de pdo ao deletar dados do cliente:{$e->getMessage()}";
		}catch(Exception $e){
			echo "Erro geral:{$e->getMessage()}";
		}
    }
}
?>